// Require the framework and instantiate it
const fastify = require("fastify")({ logger: true });

var student = {};

// Declare a route
fastify.get("/", (request, reply) => {
  reply.send("Server started successfully.");
});

// Post function
fastify.post('/add', (request, reply) => {
    if (student[request.body.studentID] === undefined) {
        student[request.body.studentID] = request.body;
        reply.send(`${request.body.studentName} details added now in mark-sheet system.`);
    }
    reply.send(`${request.body.studentName} details already exist.`);
});

//Update function
fastify.post("/update", (request, reply) => {
    let keyList = Object.keys(request.body);
    const ID = request.body[keyList[0]];
    if(student[ID] !== undefined) {
        let temp = student[ID];
        for(let i = 1; i < keyList.length; i++) {
            temp[keyList[i]] = request.body[keyList[i]];
        }
        student[ID] = temp;
        reply.send(`Student with ID-${ID} mark(s) updated.`);
    }
    reply.send(`There is no such student with ID-${ID} in the mark-sheet system`);
});

//Delete function
fastify.delete('/delete', (request, reply) => {
    let ID = request.body.studentID
    if(student[ID] !== undefined) {
        delete student[ID];
        reply.send(`Student with ID-${ID} data deleted successfully.`);
    }
    reply.send(`There is no such student with ID-${ID} in the mark-sheet system`);
});

//Report function
fastify.get("/report", (request, reply) => {
    reply.send(student);
})

// Run the server!
fastify.listen(3000, (err) => {
  if (err) {
    fastify.log.error(err);
    process.exit(1);
  }
});

/*
Name : G Mugunthan Raju
College : Sathyabama Institute of Science and Technology
Roll No : 38110333
Email ID : mugunthanraju2000@gmail.com
*/
